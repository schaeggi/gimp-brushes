# gimp-brushes
_by fryk_

This is a small collection of "space" themed gimp brushes.

__This repository contains:__
- stars.gbr (Stars of different sizes)
- stars_small.gbr (Same as above but without the big bright ones)

__Example:__

![Example Image Nr. 1](https://gitlab.com/fryk/gimp-brushes/raw/master/Examples/Example1.png "Example 1 of stars.gbr and stars_small.gbr and the gimps nebula filter")

## How do you install brushes in the Gimp?
Navigate to "edit/settings/folder/brushes" to find out in which folders gimp is looking for brushes.
You can either copy the new brushes to one of the existing ( and active) folders you see there, or you can add a new custom brushfolder:
- Add new folder (button on the left)
- Select local folder where the brushes are saved (button on the right)
- Restart the Gimp

You can now find the new brushes in the gimps brush selector. 

(If you just copied your new brushes to an existing folder and did not restart the program, you may need to press the "reload brushes" button in the brush selector to see them.)